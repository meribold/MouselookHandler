## Interface: 60200
## Title: MouselookHandler
## Notes: Allows controlling when mouselook is started and stopped.
## Author: meribold (Lukas Waymann) and pwoodworth (Patrick Woodworth)
## Version: 1.3.7
## SavedVariables: MouselookHandlerDB
## OptionalDeps: Ace3
## X-Embeds: Ace3

embeds.xml

MouselookHandler.lua
